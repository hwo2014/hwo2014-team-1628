import logging
import json

from track import Track
from car import Car

msg_count = 0


class MikkoBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.prev_pos = 0
        self.cars = {}
        self.previous_profiled_index = 0
        self.previous_piece_index = 0
        # self.switch_direction = "Right"
        logging.basicConfig(filename='throttle.log', level=logging.INFO)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        # print 'Sent: %s' % msg
        self.socket.sendall(msg + "\n")

    def create_race(self):
        return self.msg("createRace",
            {
                "botId": {
                    "name": self.name,
                    "key": self.key
                },
                "trackName": "keimola",
                "password": "",
                "carCount": 1
            })

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def switch(self, direction):
        print('switch message sent %s %s %s %s %s %s') % (
            direction, self.my_car.piece_index,
            self.my_car.is_switching, self.my_car.has_switched,
            self.my_car.can_switch, self.my_car.should_switch)
        self.msg("switchLane", direction)

    def throttle(self, throttle):
        # if (
        #     self.my_car.profiled_piece_index == 4
        #     or self.my_car.profiled_piece_index == 5
        #     or self.my_car.profiled_piece_index == 6
        #    ):
        logging.info('%s, %s' % (throttle, self.my_car.profiled_piece_index))
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.create_race()
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def calculate_throttle(self):
        target_speed = self.get_target_speed()
        # target_speed = 6
        return target_speed/float(10)

    def switch_lane(self):
        if self.my_car.can_switch and self.my_car.should_switch:
            if not self.my_car.is_switching and not self.my_car.has_switched:
                self.switch(self.my_car.switch_direction)
                # print(self.track.get_next_curve(self.my_car.profiled_piece_index + 1))
                # if self.switch_direction == "Right":
                    # self.switch_direction = "Left"
                # else:
                    # self.switch_direction = "Right"


        # current_piece = self.track.profiled_pieces[
        #     self.my_car.profiled_piece_index
        # ]
        # direction = current_piece.check_lane_switch(self.my_car)
        # if direction:
        #     self.pending_switch = True
        #     self.switch(direction)

    def on_car_positions(self, data):
        global msg_count
        msg_count += 1
        self.save_car_data(data)
        # if self.my_car.start_lane != self.my_car.end_lane:
            # print 'different lanes'
        self.switch_lane()
        if self.previous_profiled_index != self.my_car.profiled_piece_index:
            self.track.profiled_pieces[
                self.previous_profiled_index
            ].update_friction()
        self.previous_profiled_index = self.my_car.profiled_piece_index
        self.previous_piece_index = self.my_car.piece_index
        new_throttle = self.calculate_throttle()
        self.throttle_value = new_throttle
        self.throttle(new_throttle)

    def on_crash(self, data):
        print '%s%s%s' % (
            "{:10.2f}".format(self.my_car.speed),
            "{:10.2f}".format(self.my_car.angle),
            "{:10.2f}".format(self.throttle_value),
            )
        print("Someone crashed\n")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        print(repr(self.my_car))
        self.ping()

    def get_target_speed(self):
        current_piece = self.track.profiled_pieces[
            self.my_car.profiled_piece_index]
        target_speed = current_piece.get_target_speed(self.my_car)
        # if msg_count % 10 == 0:
        #     print target_speed
            # print('%s' % (self.my_car.speed))
        return target_speed

    def save_car_data(self, data):
        for car_dict in data:
            car = self.cars[str(car_dict['id']['color'])]
            car.update_info(car_dict, self.track)
            # if msg_count % 30 == 0:
                # print(repr(car))

    def save_own_car_color(self, data):
        self.color = data['color']
        print('My Car: %s' % self.color)

    def game_init(self, data):
        pieces, lanes, name, track_id = self._get_track_info(data)
        self.track = Track(pieces, lanes, name, track_id)
        self.track.create_track_pieces()
        self.set_cars(data)
        print repr(self.track)

    def on_turbo_available(self, data):
        print data

    @property
    def my_car(self):
        return self.__my_car

    def set_cars(self, data):
        for car_dict in data['race']['cars']:
            name, color, guide_flag_position, is_me, width, length =\
                self._get_car_variables(car_dict)
            new_car =\
                Car(color, name, is_me, length, width, guide_flag_position)
            if is_me:
                self.__my_car = new_car
            self.cars[color] = new_car

    def lap_finished(self, data):
        print('Lap Finished')
        # print repr(self.track)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.save_own_car_color,
            'gameInit': self.game_init,
            'lapFinished': self.lap_finished,
            'turboAvailable': self.on_turbo_available,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            # print "received: %s" % msg
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                # self.game_tick = msg['gameTick']
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

    def _get_track_info(self, data):
        track_id = data['race']['track']['id']
        name = data['race']['track']['name']
        pieces = data['race']['track']['pieces']
        lanes = data['race']['track']['lanes']
        return pieces, lanes, name, track_id

    def _get_car_variables(self, car):
        color = car['id']['color']
        is_me = color == self.color
        name = car['id']['name']
        length = car['dimensions']['length']
        width = car['dimensions']['width']
        guide_flag_position =\
            car['dimensions']['guideFlagPosition']
        return name, color, guide_flag_position, is_me, width, length
