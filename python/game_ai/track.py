import math

import params
import mikko_bot

defs = params.defaults

class Track(object):
    """Contains the Track info and some logic"""
    def __init__(self, pieces, lanes, name, track_id):
        self.track_id = track_id
        self.name = name
        self.pieces = pieces
        self.lanes = lanes
        self.profiled_pieces = []

    def __repr__(self):
        ret_string = str(self.lanes)
        counter = 0
        for piece in self.profiled_pieces:
            counter += 1
            ret_string += 'Piece %s: %s\n' % (counter, repr(piece))
        return ret_string

    def append_with_index(self, temp_pieces):
        for index, piece in enumerate(temp_pieces):
            piece['index'] = index

    def create_track_pieces(self):
        temp_pieces = list(self.pieces)
        self.append_with_index(temp_pieces)
        current_indices = []
        current_pieces = []
        prev_piece = None
        for index, piece in enumerate(temp_pieces):
            curr_piece = piece
            is_same_part = self.belongs_to_same_part(curr_piece, prev_piece)
            if prev_piece is None or is_same_part:
                current_indices.append(index)
                current_pieces.append(piece)
                prev_piece = curr_piece
            else:
                self.create_piece(index, prev_piece, curr_piece, current_pieces)
                self.reset_and_append_lists(index, curr_piece, current_pieces)
                prev_piece = curr_piece
            if index == (len(temp_pieces) - 1):
                self.create_piece(index, prev_piece, curr_piece, current_pieces)
                prev_piece = curr_piece
        self.merge_first_and_last()
        self.append_pieces()

    def append_pieces(self):
        for piece in self.profiled_pieces:
            piece.append_piece()

    def merge_first_and_last(self):
        first_piece = self.profiled_pieces[0]
        last_piece = self.profiled_pieces[-1]
        if (
                hasattr(first_piece, 'length') and hasattr(last_piece, 'length')
           ) or (
                hasattr(first_piece, 'radius')
                and hasattr(last_piece, 'radius')
                and (first_piece.radius == last_piece.radius)
                and (first_piece.angle < 0 == last_piece.angle < 0)
           ):
            first_piece.merge_with_last(last_piece)
            self.profiled_pieces.pop()

    def create_piece(self, index, prev_piece, curr_piece, current_pieces):
        new_piece = None
        args = [self, list(current_pieces), list(self.lanes)]
        if 'length' in prev_piece:
            new_piece = Straight(*args)
        else:
            new_piece = Curve(*args)
        self.profiled_pieces.append(new_piece)

    def reset_and_append_lists(self, index, curr_piece, current_pieces):
        del current_pieces[:]
        current_pieces.append(curr_piece)

    def belongs_to_same_part(self, curr_piece, prev_piece):
        if not prev_piece:
            return True
        elif 'length' in curr_piece and 'length' in prev_piece:
            return True
        elif (
                ('angle' in curr_piece)
                and ('angle' in prev_piece)
                and ((curr_piece['angle'] < 0) == (prev_piece['angle'] < 0))
                and (curr_piece['radius'] == prev_piece['radius'])
            ):
            return True
        else:
            return False

    def get_piece_type(self, piece):
        curr_type = None
        is_straight = 'length' in piece
        if is_straight:
            curr_type = 'straight'
        else:
            curr_type = 'curve'
        return curr_type

    def get_current_piece_index(self, index):
        for profiled_index, profiled_piece in enumerate(self.profiled_pieces):
            if profiled_piece.index_in_piece(index):
                return profiled_index

    def get_next_piece(self, track_piece):
        for index, piece in enumerate(self.profiled_pieces):
            if piece == track_piece:
                if not index == len(self.profiled_pieces) - 1:
                    return index + 1, self.profiled_pieces[index + 1]
                else:
                    return 0, self.profiled_pieces[0]
        raise Exception('Something went horribly wrong')

    def get_piece_info(self, in_piece_distance, end_lane, piece_index):
        profiled_piece_index =\
            self.get_current_piece_index(piece_index)
        profiled_in_piece_distance, remaining_distance_in_piece =\
            self.profiled_pieces[
                profiled_piece_index
            ].get_position_in_piece(piece_index, in_piece_distance,
                                    lane=end_lane)
        return (remaining_distance_in_piece, profiled_in_piece_distance,
                profiled_piece_index)

    def get_next_curve(self, starting_index):
        if starting_index == len(self.profiled_pieces):
            starting_index = 0
        ordered_pieces = self.profiled_pieces[starting_index:]\
            + self.profiled_pieces[:starting_index]
        for piece in ordered_pieces:
            if hasattr(piece, 'angle'):
                return piece
        raise Exception('this should not happen')

    def get_distances_to_next_switch(self, starting_index):
        ordered_pieces = self.pieces[starting_index:]\
            + self.pieces[:starting_index]
        lane_lengths = {}
        for lane in self.lanes:
            lane_lengths[lane['index']] = 0
        for piece in ordered_pieces:
            if 'length' in piece:
                for lane in lane_lengths:
                    lane_lengths[lane] += piece['length']
            elif 'angle' in piece:
                for lane in lane_lengths:
                    radius_adjustment = self.lanes[lane]['distanceFromCenter']
                    if piece['angle'] > 0:
                        radius_adjustment = -radius_adjustment
                    radius = piece['radius'] + radius_adjustment
                    length = self.get_curve_length(radius, piece['angle'])
                    lane_lengths[lane] += length
            if 'switch' in piece and piece['switch'] is True:
                break
        return lane_lengths

    def get_curve_length(self, radius, angle):
        length = abs(angle/360*math.pi*radius*2)
        return length


class BaseTrackComponent(object):
    def __init__(self, track, orig_pieces, orig_lanes):
        super(BaseTrackComponent, self).__init__()
        self.track = track
        self.friction = defs['friction']
        self.switch_indices = []
        self.pieces = {}
        self.orig_pieces = orig_pieces
        self.orig_lanes = orig_lanes

    def index_in_piece(self, index):
        return index in self.pieces

    def merge_with_last(self, last_piece):
        self.orig_pieces = last_piece.orig_pieces + self.orig_pieces


    def check_lane_switch(self, car):
        switch_direction = None
        current_piece = self.track.profiled_pieces[car.profiled_piece_index]
        has_switches = self.check_has_switches(current_piece)
        if has_switches:
            next_curve = self.get_next_curve(car)
            # print next_curve
            is_left_turn = next_curve.angle < 0
            if is_left_turn:  # and car.end_lane > min(self.track.lanes):
                switch_direction = "Left"
            elif not is_left_turn:  # and car.end_lane < max(self.track.lanes):
                switch_direction = "Right"
            # print car.piece_index, switch_direction,
            #is_left_turn, car.end_lane
        return switch_direction

    def check_has_switches(self, current_piece):
        has_switches = False
        pieces = current_piece.pieces
        for piece_index in pieces:
            has_switches = has_switches or pieces[piece_index]['switch']
        if has_switches:
            current_piece_index = None
            for index, piece in enumerate(self.track.profiled_pieces):
                if current_piece == piece:
                    current_piece_index = index
            print "switches found in piece %s" % current_piece_index
        return has_switches

    def is_angle(self):
        return hasattr(self, 'angle')


class Straight(BaseTrackComponent):
    """docstring for Straight"""
    def __init__(self, track, orig_pieces, orig_lanes):
        super(Straight, self).__init__(track, orig_pieces, orig_lanes)
        self.max_speed_point = defs['max_speed_point']
        self.length = 0
        self.is_straight = True

    def __repr__(self):
        ret_string = 'Straight: \nLength: %s\nPieces: %s\n' % (
            self.length, self.pieces
        )
        return ret_string

    def get_position_in_piece(self, index, in_piece_position, **kwargs):
        distance_from_start =\
            self.pieces[index]['distance_from_start'] + in_piece_position
        distance_remaining = self.length - distance_from_start
        return distance_from_start, distance_remaining

    def append_piece(self):
        for piece in self.orig_pieces:
            has_switch = 'switch' in piece
            piece_length = piece['length']
            self.pieces[piece['index']] = {
                'distance_from_start': int(self.length),
                'length': piece_length,
                'switch': has_switch
            }
            self.length += piece_length
            if has_switch:
                self.switch_indices.append(piece['index'])

    def update_friction(self):
        pass

    def get_target_speed(self, car):
        next_piece_target_speed = self.get_next_piece_target_speed(car)
        target_speed_in_piece =\
            self.get_calculated_target_speed(car, next_piece_target_speed)
        angle_adjustment = self.get_angle_adjustment(car)
        target_speed = target_speed_in_piece + angle_adjustment
        return min(10, max(0, target_speed))

    def get_calculated_target_speed(self, car, next_piece_target_speed):
        ##########
        # CHANGE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ##########
        lap = car.lap
        is_last_lap = False

        max_speed = defs['max_speed']
        remaining_length = car.remaining_distance_in_piece
        current_speed = car.speed
        speed_diff = current_speed - next_piece_target_speed
        adjusted_slow_down_point = self.max_speed_point*self.friction

        if (remaining_length > adjusted_slow_down_point) or is_last_lap:
            target_speed = max_speed
        else:
            for br_point in defs['breaking_points']:
                speed_diffs = br_point['speed_differences']
                if remaining_length > br_point['distance']:
                    for sd in speed_diffs:
                        if speed_diff >= sd:
                            target_speed = (next_piece_target_speed
                                            - speed_diff*speed_diffs[sd])
                            break
                if speed_diff < 0:
                    target_speed = (next_piece_target_speed
                                    + speed_diff*br_point['positive_factor'])
                    break
                else:
                    # print('I thought this wouldn\'t happen')
                    target_speed = next_piece_target_speed

        return target_speed

    def get_next_piece_target_speed(self, car):
        next_index, next_piece = self.track.get_next_piece(self)
        next_piece_target_speed = next_piece.get_target_speed(car)
        return next_piece_target_speed

    def get_angle_adjustment(self, car):
        angle_adjustment = 0
        if abs(car.angle) > defs['max_car_angle_before_adjustment']:
            angle_adjustment = defs['max_car_angle_adjustment']
        return angle_adjustment


class Curve(BaseTrackComponent):
    """docstring for Curve"""
    def __init__(self, track, orig_pieces, orig_lanes):
        super(Curve, self).__init__(track, orig_pieces, orig_lanes)
        self.angle = 0
        self.orig_lanes = orig_lanes
        self.lanes = {}
        self.radius = None
        self.is_straight = False
        self.temp_max_car_angle = None
        self.max_car_angle = defs['stable_angle']

    def __repr__(self):
        ret_string = 'Curve: \nAngle: %s\nRadius: %s\n, Friction: %s\nPieces: %s\nLanes: %s\n' % (
            self.angle, self.radius, self.friction, self.pieces, self.lanes
        )
        return ret_string

    def get_position_in_piece(self, index, in_piece_position, lane=None):
        #, msg_count=None):
        angle_from_start = self.pieces[index]['angle_from_start']
        radius = self.lanes[lane]['radius']
        distance_to_piece_start = abs(angle_from_start)/360*math.pi*radius*2
        distance_from_start = distance_to_piece_start + in_piece_position
        distance_remaining = self.lanes[lane]['length'] - distance_from_start
        # if msg_count % 5 == 0:
        #     print('Angle fs: %s, Radius: %s, Distance tps: %s, IPP: %s, DFS: %s, Len: %s' % (
        #         angle_from_start, radius, distance_to_piece_start, in_piece_position,
        #         distance_from_start, self.lanes[lane]['length']))
        return distance_from_start, distance_remaining

    def get_target_speed(self, car):
        lane_index = car.end_lane
        car_angle = car.angle
        lane_radius = self.lanes[lane_index]['radius']
        speed_default = defs['default_curve_target_speed']
        self.set_max_angle(car_angle)
        adjustments = self.get_adjustments(car, lane_radius, speed_default)
        # if mikko_bot.msg_count % 10 == 0:
        #     print adjustments
        target_speed = speed_default + sum(adjustments)
        speed_difference = car.speed - target_speed
        if speed_difference > 0:
            target_speed -=\
                speed_difference*defs['curve_break_factor']
        else:
            target_speed -= speed_difference*defs['curve_acceleration_factor']
        return min(10, max(0, target_speed))

    def get_adjustments(self, car, lane_radius, speed_default):
        adjustments = []
        adjustments.append(self.get_friction_adjustment(speed_default))
        adjustments.append(self.get_curve_angle_adjustment())
        adjustments.append(self.get_radius_adjustment(lane_radius))
        adjustments.append(self.get_next_piece_adjustment())
        adjustments.append(self.get_car_angle_adjustment(car, speed_default))
        # adjustments.append(self.get_out_of_angle_acceleration_adjustment(car))
        return adjustments

    def get_car_angle_adjustment(self, car, speed_default):
        ''' if the car's angle exceeds a max value, then slow down
            if it's below a minimum, speed up '''

        angle_adjustment = 0
        if abs(car.angle) > defs['max_car_angle_before_adjustment']:
            angle_adjustment = -defs['max_car_angle_adjustment']
        if abs(car.angle) < defs['min_car_angle_before_adjustment']:
            angle_adjustment = defs['min_car_angle_adjustment']
        return 0

    def get_next_piece_adjustment(self):
        ''' when coming out of a curve, slow down if the next piece is a
            curve in the other direction '''
        next_index, next_piece = self.track.get_next_piece(self)
        adjustment = 0
        if (
                hasattr(next_piece, 'angle')
                and (self.angle < 0) != (next_piece.angle < 0)
                and abs(next_piece.angle > defs[
                    'different_direction_turn_min_angle'
                ])):
            adjustment = -defs['different_direction_turn_adjustment']
        return adjustment

    def set_max_angle(self, car_angle):
        if self.temp_max_car_angle:
            self.temp_max_car_angle =\
                max(abs(car_angle), abs(self.temp_max_car_angle))
        else:
            self.temp_max_car_angle = abs(car_angle)

    def get_radius_adjustment(self, lane_radius):
        ''' adjust speed based on radius '''
        rad_deviation = -(defs['default_radius'] - lane_radius)
        rad_adjustment_factor = 0
        if rad_deviation < 0:
            rad_adjustment_factor = defs['negative_radius_adjustment_factor']
        else:
            rad_adjustment_factor = defs['positive_radius_adjustment_factor']
        radius_adjustment = rad_deviation*rad_adjustment_factor
        return radius_adjustment

    def get_curve_angle_adjustment(self):
        angle_adjustment = (
            defs['default_angle_length'] - abs(self.angle)
        )*defs['angle_adjustment_factor']
        return angle_adjustment

    def get_friction_adjustment(self, speed_default):
        friction_adjustment = (self.friction-1)*speed_default
        return friction_adjustment

    def get_out_of_angle_acceleration_adjustment(self, car):
        out_of_angle_acceleration = 0
        if (
            self.track.profiled_pieces[car.profiled_piece_index].is_angle()
            and car.remaining_distance_in_piece < defs[
                'out_of_curve_distance_remaining']
            and car.angle < defs['out_of_curve_max_angle']):
            out_of_angle_acceleration = defs['out_of_curve_added_force']
        return out_of_angle_acceleration

    def update_friction(self):
        self.max_car_angle = self.temp_max_car_angle
        if self.max_car_angle < defs['stable_angle']:
            self.friction = (self.friction
             - (defs['stable_angle'] - self.max_car_angle)*0.01
            )
        # pass

    def append_piece(self):
        for piece in self.orig_pieces:
            piece_angle = piece['angle']
            is_left_turn = piece_angle < 0
            if not self.radius:
                self.radius = piece['radius']
                self.create_lanes(is_left_turn)
            has_switch = 'switch' in piece
            self.pieces[piece['index']] = {
                'angle_from_start': self.angle,
                'angle': piece_angle,
                'switch': has_switch
            }
            self.angle += piece_angle
            if has_switch:
                self.switch_indices.append(piece['index'])
        for lane in self.lanes:
            radius = self.lanes[lane]['radius']
            length = self.track.get_curve_length(radius, self.angle)
            self.lanes[lane]['length'] = length


    def create_lanes(self, is_left_turn):
        for lane in self.orig_lanes:
            lane_radius = 0
            if is_left_turn:
                lane_radius = self.radius + lane['distanceFromCenter']
            else:
                lane_radius = self.radius - lane['distanceFromCenter']
            self.lanes[lane['index']] = {
                'radius': lane_radius,
            }



