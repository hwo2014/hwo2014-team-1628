class Car(object):
    """docstring for Car"""
    def __init__(self, color, name, is_me, length, width, guide_flag_position):
        self.color = color
        self.name = name
        self.is_me = is_me
        self.length = length
        self.width = width
        self.guide_flag_position = guide_flag_position
        self.previous_ipd = None
        self.piece_index = 0
        # set dummy values
        self.start_lane = -1
        self.end_lane = -1

    def __repr__(self):
        ret_string = 'Color: %s\nAngle: %s deg\nPiece: %s\nIn Piece Distance: %s\nRemaining Distance in Piece: %s\nStart Lane: %s\nEnd Lane: %s\nLap: %s\nSpeed: %s\n' % (
            self.color,
            self.angle,
            self.profiled_piece_index,
            self.profiled_in_piece_distance,
            self.remaining_distance_in_piece,
            self.start_lane,
            self.end_lane,
            self.lap,
            self.speed
        )
        return ret_string

    def update_info(self, car_dict, track):
        start_lane, angle, piece_index, end_lane, in_piece_distance, lap =\
            self._get_car_variables(car_dict)
        (
            remaining_distance_in_piece,
            profiled_in_piece_distance,
            profiled_piece_index
        ) = track.get_piece_info(in_piece_distance, end_lane, piece_index)
        self._update_info(piece_index, in_piece_distance, angle, start_lane,
                          end_lane, lap, profiled_piece_index,
                          profiled_in_piece_distance,
                          remaining_distance_in_piece, track)

    def _update_info(self, piece_index, in_piece_distance, angle, start_lane,
                     end_lane, lap, profiled_piece_index,
                     profiled_in_piece_distance, remaining_distance_in_piece,
                     track):
        # if previous piece index is different and is_switching true before
        # update, then the car switched
        self.has_switched = self.piece_index == piece_index
        self.piece_index = piece_index
        self.is_switching = self.start_lane != self.end_lane
        is_last = piece_index == (len(track.pieces) - 1)
        if is_last:
            next_index = -1  # this way it becomes 0
        else:
            next_index = piece_index + 1
        self.can_switch = ('switch' in track.pieces[next_index]
                           and track.pieces[next_index]['switch'] is True)
        self._set_should_switch(track, next_index)
        self.angle = angle
        self.current_ipd = in_piece_distance
        self.start_lane = start_lane
        self.end_lane = end_lane
        self.lap = lap
        self.profiled_piece_index = profiled_piece_index
        self.profiled_in_piece_distance = profiled_in_piece_distance
        self.remaining_distance_in_piece = remaining_distance_in_piece
        self.speed = self.set_speed(in_piece_distance)

    def _set_should_switch(self, track, piece_index):
        distances_to_next_switch =\
            track.get_distances_to_next_switch(piece_index + 1)
        shortest_lane_index = None
        shortest_distance = None
        # if all distances are equal until next switch,
        # there's no point in switching
        all_distances_equal = True
        for lane_index in distances_to_next_switch:
            current_distance = distances_to_next_switch[lane_index]
            if shortest_distance == None:
                shortest_distance = current_distance
            else:
                all_distances_equal = (
                    shortest_distance == current_distance
                    and all_distances_equal)
            if (shortest_lane_index is None
                or distances_to_next_switch[lane_index]
                < distances_to_next_switch[shortest_lane_index]):
                shortest_lane_index = lane_index
        self.should_switch = False

        if not all_distances_equal:
            self.switch_direction = None
            if self.end_lane < shortest_lane_index:
                self.should_switch = True
                self.switch_direction = "Right"
            elif self.end_lane > shortest_lane_index:
                self.should_switch = True
                self.switch_direction = "Left"
        # if self.can_switch and self.has_switched:   # and self.should_switch:
            # print distances_to_next_switch
        #     print self.end_lane, shortest_lane_index, self.should_switch, self.switch_direction

    def _get_car_variables(self, car_dict):
        angle = car_dict['angle']
        piece_index = car_dict['piecePosition']['pieceIndex']
        in_piece_distance = car_dict['piecePosition']['inPieceDistance']
        lap = car_dict['piecePosition']['lap']
        start_lane = car_dict['piecePosition']['lane']['startLaneIndex']
        end_lane = car_dict['piecePosition']['lane']['endLaneIndex']
        return start_lane, angle, piece_index, end_lane, in_piece_distance, lap

    def set_speed(self, in_piece_distance):
        if not self.previous_ipd:
            speed = 0
        elif in_piece_distance < self.previous_ipd:
            speed = self.speed
        else:
            speed = in_piece_distance - self.previous_ipd
        self.previous_ipd = in_piece_distance
        return speed

