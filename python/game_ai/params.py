defaults = {
    # for friction adjustment calculations
    'friction': 1,
    'stable_angle': 30,
    # the default remaining distance where the car needs to start slowing down
    'max_speed_point': 250,
    'max_speed': 10,
    'breaking_points': [
        {
            'distance': 100,
            'speed_differences': {
                1.5:    1.5,
                0:      0,
            },
            'positive_factor': 0.9
        },
        {
            'distance': 0,
            'speed_differences': {
                1.5:    1.5,
                0.5:    1,
                0:      0.5,
            },
            'positive_factor': 0.9
        }
    ],

    ####################################
    # adjustment parameters for curves #
    ####################################
    # this should be good a 100 radius, 180 degree curve
    'default_curve_target_speed': 6.6,

    # used for calculating the default speed
    'default_radius': 100,
    # every 1 unit of radius increases/decreases the speed by this much
    'negative_radius_adjustment_factor': 0.036,
    # adjust speed based on the length of the curve
    'positive_radius_adjustment_factor': 0.04,
    # adjust speed based on the length of the curve
    'default_angle_length': 180,
    # the higher the number, the faster the car will go through non-tight curves
    'angle_adjustment_factor': 0.002,
    # slow down if the curve following a curve is in the other direction
    'different_direction_turn_adjustment': 0.7,
    'different_direction_turn_min_angle': 45,
    # for last_second adjustments based on car angle
    'max_car_angle_before_adjustment': 50,
    'max_car_angle_adjustment': 2,
    'min_car_angle_before_adjustment': 20,
    'min_car_angle_adjustment': 0.2,
    'curve_break_factor': 1.5,
    'curve_acceleration_factor': 1.25,
    # out of curve acceleration parameters
    'out_of_curve_distance_remaining': 20,
    'out_of_curve_accel_max_angle': 20,
    'out_of_curve_max_angle': 45,
    'out_of_curve_added_force': 0.5,

}
